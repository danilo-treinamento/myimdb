﻿using Microsoft.AspNetCore.Mvc;
using Myimdb.Models.ViewModels;

namespace Myimdb.Controllers
{
    public class MovieController : Controller
    {
        public ActionResult Index(string msg = null)
        {
            
            var movies = Movie.SelectAll().ConvertAll(movie => new MovieListViewModel { Rank = movie.Rank, Title = movie.Title, Year = movie.Year });

            ViewBag.Message = msg;
            return View(movies);
        }

        public ActionResult MovieOfTheResult()
        {
            return View();
        }

        public IActionResult MovieOfTheMonthResult()
        {
            var movieOfTheMonth = new MovieCreateViewModel{ Rank = 1, Title = "The Shawshank Redemption", Year = 1994 , Storyline = "Two imprisoned men bond over a number of years..." };
            return View(movieOfTheMonth);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(MovieCreateViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            return RedirectToAction(nameof(Index), new
            {
                msg = "Movie created with success."
            });
        }
        
    }
}
